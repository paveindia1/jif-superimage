import os
import asyncio as aio
from pathlib import Path

import uvicorn
from fastapi import FastAPI, Request, Response
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel, Field
from typing import List, Callable, Set, Dict, Awaitable, Any, Union

DEBUG = os.environ.get("DEBUG", "True") == "True"
APP_PORT = os.environ.get("APP_PORT", 4000)

SRC_DIR = Path(__file__).parent
PROJ_DIR = SRC_DIR.parent
ROOT_DIR = PROJ_DIR.parent


