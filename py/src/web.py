from .prelude import *
from .models import HelloWorld

FRONTEND_DIR = (ROOT_DIR / "js" / "public" ).resolve()
TEMPLATES = Jinja2Templates(str(FRONTEND_DIR))

app = FastAPI()

app.mount("/build", StaticFiles(directory=str(FRONTEND_DIR / "build")), name="build")
app.mount("/static", StaticFiles(directory=str(FRONTEND_DIR / "static")), name="build")

@app.get("/hello")
async def hello_world():
	return HelloWorld(
		msg="Hello, World!",
		success=True
	)

@app.get("/{rest_of_route:path}")
async def frontend(req: Request, rest_of_route:str = None):
	return TEMPLATES.TemplateResponse("index.html", context={"request": req})

@app.on_event("startup")
async def startup_task():
	print("Executing startup tasks")

@app.on_event("shutdown")
async def shutdown_task():
	print("Executing shutdown task")
