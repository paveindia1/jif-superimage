from .prelude import APP_PORT, DEBUG

if __name__ == "__main__":
	import uvicorn
	uvicorn.run("src.web:app", host="0.0.0.0", port=int(APP_PORT), reload=DEBUG)

