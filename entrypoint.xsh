#!/bin/env xonsh
import os
import pathlib as p
import re

debug = os.environ["DEBUG"] == "True"

cd /app

# patches
def patch_uvicorn_20200808():
	py_pkgs = p.Path('/app/py/.venv/lib/python3.8/site-packages')
	basereload = py_pkgs / 'uvicorn/supervisors/basereload.py'
	if not basereload.exists():
		return
	file = basereload.read_text()
	file = re.sub('(\s+def shutdown.*)(\s+)(self.process.join)', '\g<1>\g<2>self.process.terminate()\g<2>\g<3>', file)
	basereload.write_text(file)
	print("Patched uvicorn.supervisors.basereload")

if debug:
	mkdir -p /app/js/public/build
	patch_uvicorn_20200808()

echo "Executing entrypoint.xsh finished..."


if len($ARGS) > 1:
	$[@($ARGS[1:])]
