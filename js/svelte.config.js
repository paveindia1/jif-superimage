const sveltePreprocess = require('svelte-preprocess');

const production = process.env.NODE_ENV == 'production'
process.env.TAILWIND_MODE =  production ? 'build' : 'watch'

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// enable run-time checks when not in production
	dev: !production,
	// we'll extract any component CSS out into
	// a separate file - better for performance
	css: css => {
		css.write('bundle.css');
	},
	preprocess: sveltePreprocess({
		postcss: true,
		scss: true,
		typescript: true,
		defaults: {
			script: "typescript",
			style: "postcss"
		}
	}),
}

module.exports = config;
