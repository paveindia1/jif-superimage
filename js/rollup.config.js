import json from '@rollup/plugin-json';
import svelte from 'rollup-plugin-svelte';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import alias from "@rollup/plugin-alias";
import typescript from '@rollup/plugin-typescript';
import replace from '@rollup/plugin-replace';

const smelte=require("smelte/rollup-plugin-smelte");
const tailwindcssConfig = require("./tailwind.config");


const production = !process.env.ROLLUP_WATCH;


const config = {
	input: 'src/main.ts',
	output: {
		sourcemap: true,
		format: 'iife',
		name: 'app',
		file: 'public/build/bundle.js'
	},
	plugins: [
		alias({
			entries: [
				{
					find: "$lib",
					replacement: __dirname + '/src/lib'
				},
				{
					find: "$routes",
					replacement: __dirname + '/src/routes'
				}
			]
		}),
		replace({
			'process.env.DEBUG': process.env.DEBUG === "True",
		}),
		svelte(require('./svelte.config.js')),


		smelte({
			purge: production,
			output: "public/build/global.css", // it defaults to static/global.css which is probably what you expect in Sapper
			// postcss: [], // Your PostCSS plugins
			// whitelist: [], // Array of classnames whitelisted from purging
			// whitelistPatterns: [], // Same as above, but list of regexes
			tailwind: {
				...tailwindcssConfig
			},
		}),
		// If you have external dependencies installed from
		// npm, you'll most likely need these plugins. In
		// some cases you'll need additional configuration -
		// consult the documentation for details:
		// https://github.com/rollup/plugins/tree/master/packages/commonjs
		resolve({
			browser: true,
			dedupe: ['svelte']
		}),
		commonjs(),
		json(),
		typescript({ sourceMap: !production }),

		// In dev mode, call `npm run start` once
		// the bundle has been generated
		// !production && serve(),

		// Watch the `public` directory and refresh the
		// browser on changes when not in production
		!production && livereload({"watch": "public", "port": process.env.LIVERELOAD_PORT}),

		// If we're building for production (npm run build
		// instead of npm run dev), minify
		production && terser()
	],
	watch: {
		clearScreen: false
	}
};

export default config;
