module.exports = {
	// future: {
	//   removeDeprecatedGapUtilities: true,
	//   purgeLayersByDefault: true,
	//   defaultLineHeights: true,
	//   standardFontWeights: true,
	// },
	// experimental: {
	//   darkModeVariant: true,
	// },
	// dark: 'class', // This can be 'media' if preferred.
	purge: {
		content: ['./src/**/*.{html,js,svelte,ts}'],
		options: {
			defaultExtractor: (content) => [
				// If this stops working, please open an issue at https://github.com/svelte-add/tailwindcss/issues rather than bothering Tailwind Labs about it
				...tailwindExtractor(content),
				// Match Svelte class: directives (https://github.com/tailwindlabs/tailwindcss/discussions/1731)
				...[...content.matchAll(/(?:class:)*([\w\d-/:%.]+)/gm)].map(
					([_match, group, ..._rest]) => group
				)
			],
			keyframes: true
		}
	},
	theme: {
		extend: {
			colors: {
				svelte: '#ff3e00',
			},
		},
	},
	variants: {},
	plugins: [
		require('@tailwindcss/typography'),
		require('tailwind-css-variables'),
	],
}
